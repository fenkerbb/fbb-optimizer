import os
import pickle as pkl
from datetime import datetime

import click
import pandas as pd
from selenium import webdriver

from fbb import League
from scrapers.projections import fangraphs
from scrapers.roster import espn

_HOST_HELP = "Host of fantasy baseball league. Currently, only 'espn' is supported"
_LEAGUE_ID_HELP = "Find this value in the URL of league's home page"


def do_scrape_league(host, league_id):
    if not os.getenv("SELENIUM_EXECUTABLE"):
        click.echo("SELENIUM_EXECUTABLE environment variable not set", err=True)
        exit(1)

    if host != "espn":
        raise NotImplementedError

    firefox_options = webdriver.FirefoxProfile()
    firefox_options.set_preference("browser.helperApps.neverAsk.saveToDisk", "text/csv")

    browser = webdriver.Firefox(
        executable_path=os.getenv("SELENIUM_EXECUTABLE"),
        firefox_profile=firefox_options,
    )
    espn.login(browser)

    opposing_teams = espn.get_opposing_teams(browser, league_id)
    key_team = espn.get_primary_team(browser, league_id)

    league = League([key_team] + opposing_teams, 1)

    batters, pitchers = fangraphs.get_stats(browser)
    browser.quit()

    for team in league.team:
        for player in team.roster:
            if player.name not in batters.index and player.name not in pitchers.index:
                print("%s not found" % player.name)
                continue
            if player.is_batter():
                player.projection.update(batters.loc[player.name].to_dict())
            if player.is_pitcher() and player.name in pitchers.index:
                player.projection.update(pitchers.loc[player.name].to_dict())
            player.calculate_derived_statistics()

    today = datetime.now().strftime("%Y-%m-%d")
    roster_dir = os.path.join(
        os.path.abspath(os.path.dirname(__file__)), "data", "rosters"
    )
    try:
        os.makedirs(roster_dir)
    except FileExistsError:
        pass

    with open(os.path.join(roster_dir, "%s.pkl" % today), "wb") as pickle:
        pkl.dump(league, pickle)
    return league


def load_or_scrape_league(host, league_id):
    today = datetime.now().strftime("%Y-%m-%d")
    filename = os.path.join(
        os.path.abspath(os.path.dirname(__file__)), "data", "rosters", "%s.pkl" % today
    )
    if not os.path.exists(filename):
        do_scrape_league(host, league_id)
    with open(filename, "rb") as pkl_file:
        return pkl.load(pkl_file)


@click.group()
def cli():
    pass


@click.command()
@click.argument("host")
@click.option("--league-id", help=_LEAGUE_ID_HELP)
@click.option("--team-name")
def show_team(host, league_id, team_name):
    """Show current team's roster and projected rest-of-season projected statistics on stdout. Currently, the only supported `host` is ESPN."""
    league = load_or_scrape_league(host, league_id)

    team_of_interest = league.get_team(team_name)
    team_of_interest.show()


@click.command()
@click.argument("host")
@click.option("--league-id", help=_LEAGUE_ID_HELP)
def scrape_league(host, league_id):
    """Download today's rosters for given league. Currently, the only supported `host` is ESPN."""
    league = do_scrape_league(host, league_id)
    return league


@click.command()
@click.argument("host")
@click.option("--league-id", help=_LEAGUE_ID_HELP)
def show_league_projections(host, league_id):
    """Scrape rosters and projections for every team in the league. Optimize batters by OPS for each team and rank teams against one another.
    Display projected standings on stdout."""
    league = load_or_scrape_league(host, league_id)
    league_df = pd.DataFrame(
        [
            t.get_projected_stats(
                rules={
                    "1B": 1,
                    "2B": 1,
                    "3B": 1,
                    "SS": 1,
                    "CI": 1,
                    "MI": 1,
                    "OF": 5,
                    "UTIL": 1,
                    "CC": 1,
                }
            )
            for t in league.team
        ]
    )
    league_df = league_df.set_index("Name")
    league_df = league_df.rank(na_option="bottom")
    league_df["ERA"] = len(league_df) + 1 - league_df["ERA"]
    league_df["WHIP"] = len(league_df) + 1 - league_df["WHIP"]

    print(league_df)
    print(league_df.sum(axis=1).sort_values(ascending=False))


cli.add_command(show_league_projections)
cli.add_command(show_team)
cli.add_command(scrape_league)

if __name__ == "__main__":
    cli()
