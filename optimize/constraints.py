import re

from pulp import LpMaximize, LpSolverDefault, LpStatus, LpVariable, lpSum, LpProblem


def optimize_ops(batters, rules):
    if "C" in rules.keys():
        rules["CC"] = rules["C"]
        del rules["C"]
    problem = LpProblem("Optimize OPS", LpMaximize)

    for batter in batters:
        problem += player_appears_once_max(batter)
    for rule in lineup_rules(problem.variables(), rules):
        problem += rule

    rx = "[" + re.escape("".join([" ", "-"])) + "]"
    ops_dict = {re.sub(rx, "_", p.name): p.get_projection("OPS") for p in batters}
    # ops_dict = {'_'.join([c for c in p.name if c in [' ', '-']]): p.get_projection('OPS') for p in batters}
    problem += ops_sum(problem.variables(), ops_dict)
    print("solving?")
    print(problem)
    LpSolverDefault.msg = 1
    problem.solve()
    print(LpStatus[problem.status])
    print("SOLVED!")
    lineup = []
    for v in problem.variables():
        if v.varValue > 0.5:
            name, _, position = v.name.rpartition("_")
            name = re.sub("_", " ", name)
            print(name)
            player = [batter for batter in batters if batter.name == name][0]
            lineup.append((player, position))
    return lineup


def player_appears_once_max(player):
    arr = []
    for position in player.positions:
        if position == "C":
            position = "CC"
        arr.append(LpVariable("%s_%s" % (player.name, position), cat="Binary"))
    if "1B" in player.positions or "3B" in player.positions:
        arr.append(LpVariable("%s_%s" % (player.name, "CI"), cat="Binary"))
    if "2B" in player.positions or "SS" in player.positions:
        arr.append(LpVariable("%s_%s" % (player.name, "MI"), cat="Binary"))
    arr.append(LpVariable("%s_%s" % (player.name, "UTIL"), cat="Binary"))
    return lpSum(arr) <= 1


def lineup_rules(variables, rules):
    arr = []
    for position, number in rules.items():
        arr.append(lpSum([v for v in variables if position in v.name]) <= number)
    return arr


def ops_sum(variables, ops_dict):
    arr = []
    for v in variables:
        arr.append(v * ops_dict[v.name.rpartition("_")[0]])
    return lpSum(arr)
