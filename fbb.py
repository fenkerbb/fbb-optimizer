""" Copyright 2019 Benjamin Fenker """
from collections import defaultdict
from dataclasses import dataclass, field
from typing import Dict, List

import numpy as np
import pandas as pd

from optimize import constraints

BATTER_POSITIONS = {"C", "1B", "2B", "SS", "3B", "LF", "CF", "RF", "OF", "DH"}
PITCHER_POSITIONS = {"SP", "RP"}
QS_SCALING = 4.12

pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 1000)


@dataclass
class Player:
    name: str
    positions: List[str]
    projection: Dict = field(default_factory=dict)
    is_injured: bool = False

    def is_batter(self):
        return len(set(self.positions) & BATTER_POSITIONS) > 0

    def is_pitcher(self):
        return len(set(self.positions) & PITCHER_POSITIONS) > 0

    def is_two_way(self):
        return self.is_batter() and self.is_pitcher()

    def get_projection(self, stat):
        try:
            return self.projection[stat]
        except KeyError:
            return 0

    def calculate_derived_statistics(self):
        if self.is_pitcher():
            if "SV" in self.projection.keys() and "HLD" in self.projection.keys():
                self.projection["SVHD"] = self.projection["SV"] + self.projection["HLD"]
            self.projection["QS"] = self.get_quality_starts()

    def get_quality_starts(self):
        if not self.is_pitcher():
            return 0
        projection = self.projection
        if (
            "ER" not in projection.keys()
            or "GS" not in projection.keys()
            or "G" not in projection.keys()
            or "IP" not in projection.keys()
        ):
            return 0

        if projection["ER"] < 1 or projection["GS"] < 1:
            return 0
        games_started = projection["GS"]
        games_pitched = projection["G"]
        ratio_started = games_started / projection["G"]

        term_1 = games_started / (projection["ER"] * ratio_started)
        term_2 = projection["IP"] * ratio_started
        term_3 = (games_started + games_pitched) / (2 * games_pitched)
        return term_1 * term_2 * term_3 / QS_SCALING


@dataclass
class Team:
    name: str
    roster: List[Player]
    lineup: List[Player] = None
    is_key: bool = False

    def show(self):
        batters = [p for p in self.roster if p.is_batter()]
        pitchers = [p for p in self.roster if p.is_pitcher()]

        batters_dict = pd.DataFrame(
            [
                {
                    "name": b.name,
                    "is_injured": b.is_injured,
                    "AB": b.get_projection("AB"),
                    "PA": b.get_projection("PA"),
                    "H": b.get_projection("H"),
                    "BB": b.get_projection("BB"),
                    "HBP": b.get_projection("HBP"),
                    "HR": b.get_projection("HR"),
                    "RBI": b.get_projection("RBI"),
                    "SB": b.get_projection("SB"),
                    "OPS": b.get_projection("OPS"),
                    "WAR": b.get_projection("WAR"),
                }
                for b in batters
            ]
        )

        pitchers_dict = pd.DataFrame(
            [
                {
                    "name": p.name,
                    "is_injured": p.is_injured,
                    "QS": p.get_projection("QS"),
                    "ERA": p.get_projection("ERA"),
                    "IP": p.get_projection("IP"),
                    "ER": p.get_projection("ER"),
                    "WHIP": p.get_projection("WHIP"),
                    "SVHD": p.get_projection("SVHD"),
                    "K/9": p.get_projection("K/9"),
                    "SO": p.get_projection("SO"),
                    "H": p.get_projection("H"),
                    "BB": p.get_projection("BB"),
                }
                for p in pitchers
            ]
        )

        print(batters_dict.sort_values(by="OPS", ascending=False))
        print(pitchers_dict.sort_values(by="ERA", ascending=True))

    def build_lineup(self, rules, method="OPS"):
        batters = [p for p in self.roster if p.is_batter() and not p.is_injured]
        lineup = constraints.optimize_ops(batters, rules)
        return lineup

    def get_projected_stats(self, rules, lineup_method="OPS"):
        lineup = self.build_lineup(rules, lineup_method)
        lineup = [l[0] for l in lineup]
        for p in self.roster:
            if p.is_pitcher() and not p.is_injured:
                lineup.append(p)
        stats = get_aggregate_statistics(lineup)
        stats["Name"] = self.name
        return stats

    @staticmethod
    def _count_excess_players_by_position(players, rules):
        available_by_position = Team._count_players_by_position(players)
        excess = defaultdict(int)

        for position, available in available_by_position.items():
            excess[position] = available - rules[position][0]
        return excess

    @staticmethod
    def _count_players_by_position(players):
        available_by_position = defaultdict(int)
        for player in players:
            for position in player.positions:
                available_by_position[position] = available_by_position[position] + 1
        return available_by_position


@dataclass
class League:
    team: List[Team]
    key_team: int

    def get_team(self, team_name):
        """
        Return specific team
        :param team_name: (str)
        :return: Team object
        """
        for t in self.team:
            if t.name == team_name:
                return t
        raise Exception


def aggregate_sum(players, key, batters=True):
    if batters:
        return np.sum(p.get_projection(key) for p in players if p.is_batter())
    return np.sum(p.get_projection(key) for p in players if p.is_pitcher())


def get_aggregate_statistics(players):
    hits = aggregate_sum(players, "H")
    at_bats = aggregate_sum(players, "AB")
    average = hits / at_bats
    plate_appearances = aggregate_sum(players, "PA")
    walks = aggregate_sum(players, "BB")
    hbp = aggregate_sum(players, "HBP")
    on_base_percentage = (hits + walks + hbp) / plate_appearances

    earned_runs = aggregate_sum(players, "ER", batters=False)
    innings_pitched = aggregate_sum(players, "IP", batters=False)

    walks = aggregate_sum(players, "BB", batters=False)
    hits = aggregate_sum(players, "H", batters=False)

    strikeouts = aggregate_sum(players, "SO", batters=False)
    try:
        team_era = 9.0 * earned_runs / innings_pitched
        whip = (walks + hits) / innings_pitched
        strikeouts_per_nine = 9.0 * strikeouts / innings_pitched
    except ZeroDivisionError:
        team_era = np.inf
        whip = np.inf
        strikeouts_per_nine = np.inf
    return {
        "HR": aggregate_sum(players, "HR"),
        "RBI": aggregate_sum(players, "RBI"),
        "SB": aggregate_sum(players, "SB"),
        "AVG": average,
        "OBP": on_base_percentage,
        "QS": aggregate_sum(players, "QS", batters=False),
        "ERA": team_era,
        "WHIP": whip,
        "K/9": strikeouts_per_nine,
        "SVHD": aggregate_sum(players, "SVHD", batters=False),
    }
