# fbb-optimizer

This is just a fun project to 1) scrape ESPN's fantasy site to download all rosters in a fantasy baseball league
and 2) scrape Fangraphs to download rest-of-season projections for every player. Combining those should ideally provide
insight into good roster moves.

## Setup

1. Python and accompanying packages
2. Selenium / Geckodriver
3. Cookies 

## Usage

To view rest-of-season projections for your ESPN-hosted team:

Note your `league-id` by signing in to  ESPN fantasy baseball and examining the URL. It should have the format http://fantasy.espn.com/baseball/team?leagueId=<league-id>&teamId=<team-id>&seasonId=<year>
```
python fbb_evaluate show-team espn\
    --league-id=<league-id>\
    --team-name="<team name>"
```

If a `cookies.pkl` file is present (and contains sufficient cookies to log in to ESPN), 
Selenium will use it to log in and scrape the roster. If not, you will have to enter your ESPN password.

To view rest-of-season projected standings based on a series of team by team projections:

```
python fbb_evaluate show-league-projections espn\
    --league-id=<league-id>
``` 