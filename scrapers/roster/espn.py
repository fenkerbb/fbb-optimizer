# Copyright 2019 (c) Benjamin Fenker
import os
import pickle as pkl

from bs4 import BeautifulSoup

from fbb import Player, Team

LOGIN_URL = "http://www.espn.com/fantasy"
URL_BASE = "http://fantasy.espn.com"


def _build_league_url(league_id):
    """
    Return formatted URL to query roster for given season, league, team
    :param league_id: (int) Numeric league ID, from URL when logged in
    """
    return URL_BASE + "/baseball/league?leagueId=%s" % league_id


def parse_roster(soup):
    """
    Return roster from ESPN team page
    :param soup: Page source with roster, processed into LXML
    :return: Array of Player objects on team
    """
    cells = soup.find_all(
        "div", {"class": "jsx-2893327412 player-column__athlete flex"}
    )

    positions = soup.find_all("span", {"class": "playerinfo__playerpos ttu"})
    positions = [str.split(p.text) for p in positions]
    positions = [[q.strip(",") for q in p] for p in positions]
    injured = [
        len(player.find_all("span", {"title": "Out: 10-day IL"})) > 0
        for player in cells
    ]
    names = [cell["title"].replace("-", " ") for cell in cells]
    return [
        Player(n, p, {}, is_injured=i) for n, p, i in zip(names, positions, injured)
    ]


def parse_name(soup):
    return soup.find_all("span", {"class": "teamName truncate"})[0]["title"]


def get_team(browser, url, is_key=False):
    """
    Scrape ESPN fantasy baseball pages for roster data. Requires manual login for now
    :param browser: (obj) E.g. Selenium
    :param url: (str) Home page for team to scrape
    :param is_key: (bool) Is team 'My Team?'
    :return: Team object corresponding to above parameters
    """
    browser.get(url)
    soup = BeautifulSoup(browser.page_source, "lxml")
    roster = parse_roster(soup)
    roster_deduped = []
    for player in roster:
        if player.name not in [p.name for p in roster_deduped]:
            roster_deduped.append(player)
    name = parse_name(soup)
    return Team(name, roster_deduped, is_key)


def get_opposing_teams(browser, league_id):
    """
    scrape league page for opposing teams. requires manual login for now
    :param browser: (obj) e.g. selenium
    :param league_id: (int) number league id, from url when logged in
    :return: list of opposing teams
    """
    browser.get(_build_league_url(league_id))
    soup = BeautifulSoup(browser.page_source, "lxml")
    teams_drop_down = soup.find("li", {"class": "teams NavSecondary__Item"})
    team_list = teams_drop_down.find_all("li", {"class": "NavMain__SubNav__Item"})
    return [get_team(browser, URL_BASE + team.find("a")["href"]) for team in team_list]


def get_primary_team(browser, league_id):
    """
    scrape league page for primary team (defined by 'My Team'). requires manual login for now
    :param browser: (obj) e.g. selenium
    :param league_id: (int) number league id, from url when logged in
    :return: primary team
    """
    browser.get(_build_league_url(league_id))
    soup = BeautifulSoup(browser.page_source, "lxml")
    href = soup.find("li", {"class": "myTeam NavSecondary__Item"}).find("a")["href"]
    return get_team(browser, URL_BASE + href, is_key=True)


def login(browser):
    browser.get(URL_BASE)

    cookie_file = "cookies.pkl"
    if not os.path.isfile(cookie_file):
        input('Login to ESPN, then "ENTER" to continue')
        pkl.dump(browser.get_cookies(), open(cookie_file, "wb"))

    cookies = pkl.load(open(cookie_file, "rb"))
    for cookie in cookies:
        browser.add_cookie(cookie)


def main():
    from selenium import webdriver

    browser = webdriver.Firefox(
        executable_path=r"C:\Users\ben\Downloads\geckodriver-v0.24.0-win64\geckodriver.exe"
    )
    print(get_opposing_teams(browser, 254778))
    browser.quit()


if __name__ == "__main__":
    main()
