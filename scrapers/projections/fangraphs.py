import json
import os
from datetime import datetime
from pathlib import Path

import pandas as pd

URL_BASE = "https://fangraphs.com/projections.aspx"


def get_options(player_type, projections):
    """
    Build get_options dict
    :param player_type: 'bat' for batter stats, 'pit' for pitcher stats
    :param projections: Projection algorithm to use
    :return: dict of get_options that Fangraphs will recognize
    """
    return {
        "pos": "all",
        "stats": player_type,
        "type": projections,
        "team": "0",  # all
        "lg": "all",
        "players": "0",  # all
    }


def get_url(options):
    return URL_BASE + "?" + "&".join({"%s=%s" % (k, v) for k, v in options.items()})


def merge_pitchers(rzips, dc):
    """

    :param rzips:
    :param dc:
    :return:
    """
    dc.loc[:, "playerid"] = dc.loc[:, "playerid"].str.replace("sa", "").astype("int")
    return rzips.merge(
        dc[["Name", "SV", "HLD", "playerid"]], on=["Name", "playerid"], how="left"
    )


def get_stats(browser):
    dictionary = json.load(
        open(
            os.path.join(os.path.abspath(os.path.dirname(__file__)), "translator.json"),
            "r",
        )
    )
    batters = get_player_stats(browser, "bat", "rzips")

    pitchers_rzips = get_player_stats(browser, "pit", "rzips")
    pitchers_dc = get_player_stats(browser, "pit", "rfangraphsdc")
    pitchers = merge_pitchers(pitchers_rzips, pitchers_dc)

    for old_val, new_val in dictionary.items():
        if old_val in batters.Name.to_list():
            print("Replacing %s with %s" % (old_val, new_val))
            batters.loc[batters.Name == old_val, "Name"] = new_val
        if old_val in pitchers.Name.to_list():
            pitchers.loc[pitchers.Name == old_val, "Name"] = new_val

    return batters.set_index("Name", drop=True), pitchers.set_index("Name", drop=True)


def get_player_stats(browser, player_type, projections):
    """
    Return dictionary of statistics?
    :param browser: (obj) e.g. Selenium
    :param player_type: 'bat' for batter stats, 'pit' for pitcher stats
    :param projections: Projection algorithm to use
    :return:
    """
    today = datetime.now().strftime("%Y-%m-%d")
    directory = os.path.join(
        os.path.abspath(os.path.dirname(__file__)),
        "..",
        "..",
        "data",
        "projections",
        today,
    )
    filename = os.path.join(directory, "%s-%s.csv" % (player_type, projections))
    if not os.path.isfile(filename):
        browser.get(get_url(get_options(player_type, projections)))
        browser.find_element_by_id("ProjectionBoard1_cmdCSV").click()

        download_directory = os.path.join(str(Path.home()), "Downloads")
        try:
            os.makedirs(directory)
        except FileExistsError:
            pass
        os.rename(
            os.path.join(download_directory, "FanGraphs Leaderboard.csv"), filename
        )

    data = pd.read_csv(filename)
    return data
